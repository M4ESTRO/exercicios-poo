package app.aplication;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class App01 {

	public static void main(String[] args) {

		Double av1, av2, av3, media;
		String status;

		/*
		 * //Modo dificil Scanner teclado = new Scanner(System.in);
		 * System.out.println("Insira a primeira nota: "); av1 = teclado.nextDouble();
		 * System.out.println("Insira a segunda nota: "); av2 = teclado.nextDouble();
		 * System.out.println("Insira a terceira nota: "); av3 = teclado.nextDouble();
		 * media = (av1+av2+av3)/3; if(media >= 7) { System.out.println("M�dia: " +
		 * media + "\nAPROVADO"); }else { System.out.println("M�dia: " + media +
		 * "\nREPROVADO"); } teclado.close();
		 */

		// Modo facil

		try {
			av1 = Double.parseDouble(JOptionPane.showInputDialog("Insira a primeira nota: "));
			av2 = Double.parseDouble(JOptionPane.showInputDialog("Insira a segunda nota: "));
			av3 = Double.parseDouble(JOptionPane.showInputDialog("Insira a terceira nota: "));
			media = (av1 + av2 + av3) / 3;

			if (media >= 7) {
				status = "APROVADO!";
			} else {
				status = "REPROVADO";
			}
			JOptionPane.showMessageDialog(null, "M�dia: " + media + "\nStatus Final: " + status);

		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Por favor, insira apenas valores NUM�RICOS!");
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido: " + e.toString());
		}

	}

}
